;; Example code for http://pestilenz.org/~ckeen/blog/lindenmayer.html
;;
;; Copyright (c) 2012, Christian Kellermann
;; All rights reserved.

;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:

;;     Redistributions of source code must retain the above copyright
;;     notice, this list of conditions and the following disclaimer.
;;
;;     Redistributions in binary form must reproduce the above
;;     copyright notice, this list of conditions and the following
;;     disclaimer in the documentation and/or other materials provided
;;     with the distribution.
;;
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
;; FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
;; COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
;; INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
;; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
;; STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
;; OF THE POSSIBILITY OF SUCH DAMAGE.

(use srfi-1 data-structures miscmacros doodle)

(define-constant *pi* 3.14159265358979)

(define-record l-system state rules)

(define (grow-system system)
  (let ((s (l-system-state system))
        (rules (l-system-rules system)))
    (string-fold (lambda (c s)
                   (cond ((alist-ref (string c) rules equal?)
                          => (lambda (n)
                               (string-concatenate (list s n))))
                         (else (string-concatenate (list s (string c))))))
                 ""
                 s)))

(define-syntax define-l-system
  (syntax-rules (->)
    ((_ name seed (var -> expansion) ...)
     (define name (make-l-system seed '(( var . expansion) ...))))))


(define (new-angle a dir)
  (modulo (+ a dir) 360))

(define (new-coords x y alpha step)
  (let ((c (* step (cos (* (/ *pi* 180) alpha))))
        (a (* step (sin (* (/ *pi* 180) alpha)))))
    (values (+ x c) (+ y a))))


(define-record turtle x y angle angle-step stepwidth stack)

(define (push-turtle-stack! t)
  (turtle-stack-set! t
                     (cons (list
                            (turtle-x t)
                            (turtle-y t)
                            (turtle-angle t)
                            (turtle-angle-step t)
                            (turtle-stepwidth t))
                           (turtle-stack t))))

(define (pop-turtle-stack! t)
  (let ((l (turtle-stack t)))
    (when (not (null? l))
      (turtle-stack-set! t (cdr l))
      (turtle-x-set! t (first (car l)))
      (turtle-y-set! t (second (car l)))
      (turtle-angle-set! t (third (car l)))
      (turtle-angle-step-set! t (fourth (car l)))
      (turtle-stepwidth-set! t (fifth (car l))))))

(define (move-turtle! t)
  (lambda (c)
    (case  c
      ((#\[) (push-turtle-stack! t))
      ((#\]) (pop-turtle-stack! t))
      ((#\+) (turtle-angle-set! t (new-angle (turtle-angle t) (- (turtle-angle-step t)))))
      ((#\-) (turtle-angle-set! t (new-angle (turtle-angle t) (turtle-angle-step t))))
      ((#\F) (let-values (((x y) (new-coords (turtle-x t)
                                             (turtle-y t)
                                             (turtle-angle t)
                                             (turtle-stepwidth t))))
                        (draw-line (turtle-x t)
                                  (turtle-y t)
                                  x
                                  y
                                  color: solid-black)
                        (turtle-x-set! t x)
                        (turtle-y-set! t y))))))


(define (draw-system s iterations #!key (x 0) (y 0) (angle 0) (angle-step 0) (step-width 0))
  (let ((t (make-turtle x y angle angle-step step-width '())))
    (string-for-each (move-turtle! t)
                     (let loop ((i iterations))
                       (cond ((> 0 i) (error "Negative number of " iterations))
                             ((= 0 i) (l-system-state s))
                             (else
                              (let ((new-state (grow-system s)))
                                (l-system-state-set! s new-state)
                                (loop (sub1 i)))))))
                                (show!)))

(define-l-system koch "F++F++F" ( "F" -> "F-F++F-F"))
(define-l-system dragon "FX" ( "X" -> "X+YF+") ( "Y" -> "-FX-Y"))
(define-l-system grass "F" ( "F" -> "F[+F]F[-F]F"))
(define-l-system plant "F" ( "F" -> "FF-[-F+F+F]+[+F-F-F]" ))


(define h 800)
(define w 800)
(define center-x (round (/ h 2)))
(define center-y (round (/ w 2)))
(new-doodle height: h width: w background: solid-white)

(draw-system plant 3 x: center-x y: center-y angle: -90 angle-step: -22.5 step-width: 8)
(save-screenshot "plant.png")
(read-char)
(clear-screen)
(draw-system dragon 12 x: center-x y: center-y angle: 0 angle-step: 90 step-width: 4)
(save-screenshot "dragon.png")
(read-char)
(clear-screen)
(draw-system koch 3 x: center-x y: center-y angle: 0 angle-step: 60 step-width: 7)
(save-screenshot "koch.png")
(read-char)
(clear-screen)
(draw-system grass 6 x: center-y y: h angle: -90 angle-step: 25.7 step-width: 2)
(save-screenshot "grass.png")
(read-char)
(clear-screen)
(quit)